namespace ASCOM.Filtre
{
    partial class SetupDialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdOK = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.picASCOM = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.focusOffset = new System.Windows.Forms.Label();
            this.filtreAdi = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.filtre1 = new System.Windows.Forms.TextBox();
            this.filtre2 = new System.Windows.Forms.TextBox();
            this.filtre3 = new System.Windows.Forms.TextBox();
            this.filtre4 = new System.Windows.Forms.TextBox();
            this.filtre5 = new System.Windows.Forms.TextBox();
            this.filtre6 = new System.Windows.Forms.TextBox();
            this.ofset1 = new System.Windows.Forms.TextBox();
            this.ofset2 = new System.Windows.Forms.TextBox();
            this.ofset3 = new System.Windows.Forms.TextBox();
            this.ofset4 = new System.Windows.Forms.TextBox();
            this.ofset5 = new System.Windows.Forms.TextBox();
            this.ofset6 = new System.Windows.Forms.TextBox();
            this.comboBoxComPort = new System.Windows.Forms.ComboBox();
            this.chkTrace = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.picASCOM)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdOK
            // 
            this.cmdOK.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOK.Location = new System.Drawing.Point(15, 332);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(150, 24);
            this.cmdOK.TabIndex = 0;
            this.cmdOK.Text = "Tamam";
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(178, 332);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(149, 25);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "�ptal";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 386);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Construct your driver\'s setup dialog here.";
            // 
            // picASCOM
            // 
            this.picASCOM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picASCOM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picASCOM.Image = global::ASCOM.Filtre.Properties.Resources.ASCOM;
            this.picASCOM.Location = new System.Drawing.Point(280, 12);
            this.picASCOM.Name = "picASCOM";
            this.picASCOM.Size = new System.Drawing.Size(48, 56);
            this.picASCOM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picASCOM.TabIndex = 3;
            this.picASCOM.TabStop = false;
            this.picASCOM.Click += new System.EventHandler(this.BrowseToAscom);
            this.picASCOM.DoubleClick += new System.EventHandler(this.BrowseToAscom);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Comm Port";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Controls.Add(this.comboBoxComPort);
            this.groupBox1.Controls.Add(this.chkTrace);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(312, 313);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtre Ayarlar�";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.Controls.Add(this.focusOffset, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.filtreAdi, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.filtre1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.filtre2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.filtre3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.filtre4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.filtre5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.filtre6, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.ofset1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.ofset2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.ofset3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.ofset4, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.ofset5, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.ofset6, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 73);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(280, 221);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // focusOffset
            // 
            this.focusOffset.AutoSize = true;
            this.focusOffset.Location = new System.Drawing.Point(163, 0);
            this.focusOffset.Name = "focusOffset";
            this.focusOffset.Size = new System.Drawing.Size(67, 13);
            this.focusOffset.TabIndex = 9;
            this.focusOffset.Text = "Focus Offset";
            // 
            // filtreAdi
            // 
            this.filtreAdi.AutoSize = true;
            this.filtreAdi.Location = new System.Drawing.Point(53, 0);
            this.filtreAdi.Name = "filtreAdi";
            this.filtreAdi.Size = new System.Drawing.Size(29, 13);
            this.filtreAdi.TabIndex = 8;
            this.filtreAdi.Text = "Filtre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Filtre 1:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Filtre 2:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Filtre 3:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Filtre 4:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Filtre 5:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Filtre 6:";
            // 
            // filtre1
            // 
            this.filtre1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "Filter1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.filtre1.Location = new System.Drawing.Point(53, 33);
            this.filtre1.Name = "filtre1";
            this.filtre1.Size = new System.Drawing.Size(100, 20);
            this.filtre1.TabIndex = 11;
            this.filtre1.Text = global::ASCOM.Filtre.Properties.Settings.Default.filter1;
            // 
            // filtre2
            // 
            this.filtre2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "filter2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.filtre2.Location = new System.Drawing.Point(53, 63);
            this.filtre2.Name = "filtre2";
            this.filtre2.Size = new System.Drawing.Size(100, 20);
            this.filtre2.TabIndex = 12;
            this.filtre2.Text = global::ASCOM.Filtre.Properties.Settings.Default.filter2;
            // 
            // filtre3
            // 
            this.filtre3.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "filter3", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.filtre3.Location = new System.Drawing.Point(53, 93);
            this.filtre3.Name = "filtre3";
            this.filtre3.Size = new System.Drawing.Size(100, 20);
            this.filtre3.TabIndex = 12;
            this.filtre3.Text = global::ASCOM.Filtre.Properties.Settings.Default.filter3;
            // 
            // filtre4
            // 
            this.filtre4.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "filter4", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.filtre4.Location = new System.Drawing.Point(53, 123);
            this.filtre4.Name = "filtre4";
            this.filtre4.Size = new System.Drawing.Size(100, 20);
            this.filtre4.TabIndex = 12;
            this.filtre4.Text = global::ASCOM.Filtre.Properties.Settings.Default.filter4;
            // 
            // filtre5
            // 
            this.filtre5.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "filter5", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.filtre5.Location = new System.Drawing.Point(53, 153);
            this.filtre5.Name = "filtre5";
            this.filtre5.Size = new System.Drawing.Size(100, 20);
            this.filtre5.TabIndex = 12;
            this.filtre5.Text = global::ASCOM.Filtre.Properties.Settings.Default.filter5;
            // 
            // filtre6
            // 
            this.filtre6.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "filer6", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.filtre6.Location = new System.Drawing.Point(53, 183);
            this.filtre6.Name = "filtre6";
            this.filtre6.Size = new System.Drawing.Size(100, 20);
            this.filtre6.TabIndex = 12;
            this.filtre6.Text = global::ASCOM.Filtre.Properties.Settings.Default.filer6;
            // 
            // ofset1
            // 
            this.ofset1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "Ofset1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ofset1.Location = new System.Drawing.Point(163, 33);
            this.ofset1.Name = "ofset1";
            this.ofset1.Size = new System.Drawing.Size(100, 20);
            this.ofset1.TabIndex = 11;
            this.ofset1.Text = global::ASCOM.Filtre.Properties.Settings.Default.Ofset1;
            // 
            // ofset2
            // 
            this.ofset2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "Ofset2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ofset2.Location = new System.Drawing.Point(163, 63);
            this.ofset2.Name = "ofset2";
            this.ofset2.Size = new System.Drawing.Size(100, 20);
            this.ofset2.TabIndex = 11;
            this.ofset2.Text = global::ASCOM.Filtre.Properties.Settings.Default.Ofset2;
            // 
            // ofset3
            // 
            this.ofset3.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "Ofset3", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ofset3.Location = new System.Drawing.Point(163, 93);
            this.ofset3.Name = "ofset3";
            this.ofset3.Size = new System.Drawing.Size(100, 20);
            this.ofset3.TabIndex = 11;
            this.ofset3.Text = global::ASCOM.Filtre.Properties.Settings.Default.Ofset3;
            // 
            // ofset4
            // 
            this.ofset4.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "Ofset4", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ofset4.Location = new System.Drawing.Point(163, 123);
            this.ofset4.Name = "ofset4";
            this.ofset4.Size = new System.Drawing.Size(100, 20);
            this.ofset4.TabIndex = 11;
            this.ofset4.Text = global::ASCOM.Filtre.Properties.Settings.Default.Ofset4;
            // 
            // ofset5
            // 
            this.ofset5.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "Ofset5", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ofset5.Location = new System.Drawing.Point(163, 153);
            this.ofset5.Name = "ofset5";
            this.ofset5.Size = new System.Drawing.Size(100, 20);
            this.ofset5.TabIndex = 11;
            this.ofset5.Text = global::ASCOM.Filtre.Properties.Settings.Default.Ofset5;
            // 
            // ofset6
            // 
            this.ofset6.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "Ofset6", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ofset6.Location = new System.Drawing.Point(163, 183);
            this.ofset6.Name = "ofset6";
            this.ofset6.Size = new System.Drawing.Size(100, 20);
            this.ofset6.TabIndex = 11;
            this.ofset6.Text = global::ASCOM.Filtre.Properties.Settings.Default.Ofset6;
            // 
            // comboBoxComPort
            // 
            this.comboBoxComPort.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ASCOM.Filtre.Properties.Settings.Default, "comPort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.comboBoxComPort.FormattingEnabled = true;
            this.comboBoxComPort.Location = new System.Drawing.Point(70, 26);
            this.comboBoxComPort.Name = "comboBoxComPort";
            this.comboBoxComPort.Size = new System.Drawing.Size(92, 21);
            this.comboBoxComPort.TabIndex = 7;
            this.comboBoxComPort.Text = global::ASCOM.Filtre.Properties.Settings.Default.comPort;
            // 
            // chkTrace
            // 
            this.chkTrace.AutoSize = true;
            this.chkTrace.Checked = global::ASCOM.Filtre.Properties.Settings.Default.traceEnabled;
            this.chkTrace.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::ASCOM.Filtre.Properties.Settings.Default, "traceEnabled", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkTrace.Location = new System.Drawing.Point(199, 30);
            this.chkTrace.Name = "chkTrace";
            this.chkTrace.Size = new System.Drawing.Size(69, 17);
            this.chkTrace.TabIndex = 6;
            this.chkTrace.Text = "Trace on";
            this.chkTrace.UseVisualStyleBackColor = true;
            // 
            // SetupDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 367);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.picASCOM);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetupDialogForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Filtre Setup";
            ((System.ComponentModel.ISupportInitialize)(this.picASCOM)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picASCOM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkTrace;
        private System.Windows.Forms.ComboBox comboBoxComPort;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label filtreAdi;
        private System.Windows.Forms.Label focusOffset;
        private System.Windows.Forms.TextBox filtre1;
        private System.Windows.Forms.TextBox filtre2;
        private System.Windows.Forms.TextBox filtre3;
        private System.Windows.Forms.TextBox filtre4;
        private System.Windows.Forms.TextBox filtre5;
        private System.Windows.Forms.TextBox ofset1;
        private System.Windows.Forms.TextBox ofset2;
        private System.Windows.Forms.TextBox ofset3;
        private System.Windows.Forms.TextBox ofset4;
        private System.Windows.Forms.TextBox ofset5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox filtre6;
        private System.Windows.Forms.TextBox ofset6;
        private System.Windows.Forms.Label label8;

    }
}